+++
title = "TEMPLATE" # chaîne identique au nom de fichier et nom d'auteur
description = "description courte" # pas de phrase
draft = true # À SUPPRIMER !

[extra]
full_name = "Templatus Lipsum" # nom complet
avatar = "avatar.svg" # image dans /static/equipe
website = "https://example.com/"
forum_duniter = "pseudo"
forum_ml = "pseudo"
g1_pubkey = "EzC9CyJvJZpbGJafXXCUqdn8VRpFbNCZUnNqntMg4hxi"
phone = "+33 6 12 34 56 78"
email = "mail@domaine.tld"
xmpp = "pseudo@domaine.tld"
gitduniter = "pseudo"
g1_map = false # (des)active le lien vers la carte ML

[taxonomies]
authors = ["TEMPLATE",] # chacun est auteur de sa propre page
+++

Description longe affichée sur la page de l'utilisateur. Format markdown, balises autorisées...