+++
title = "HugoTrentesaux"
description = "mainteneur du site duniter.fr et contributeur polyvalent"

[extra]
full_name = "Hugo Trentesaux"
avatar = "HugoTrentesaux.png"
website = "https://trentesaux.fr/"
forum_duniter = "HugoTrentesaux"
forum_ml = "Hugo-Trentesaux"
g1_pubkey = "55oM6F9ZE2MGi642GGjhCzHhdDdWwU6KchTjPzW7g3bp"
g1_map = true
phone = "+33 6 49 88 18 21"
email = "hugo@trentesaux.fr"
xmpp = "h30x@militant.es"
gitduniter = "HugoTrentesaux"
+++

J'ai découvert la ğ1 en 2017 grâce aux réseaux sociaux libres et décentralisés. J'ai contribué aux débuts de la toile francilienne et ai apporté de petites contributions techniques sur divers logiciels comme Dunitrust, Duniterpy, Ğecko, ForceAtlas2-rs... J'ai réalisé la migration du site de Duniter pour pouvoir en assurer la maintenance dans les années à venir. J'aime bien comprendre en profondeur pour documenter et vulgariser les aspects techniques.

J'ai réalisé quelques petits projets et preuves de concept comme :
- la toile de confiance animée [https://tube.p2p.legal/w/qJX3Hsfa61Y8WjZaFz7ja6](https://tube.p2p.legal/w/qJX3Hsfa61Y8WjZaFz7ja6)
- DataJune [https://git.42l.fr/HugoTrentesaux/DataJune.jl](https://git.42l.fr/HugoTrentesaux/DataJune.jl)
- Jucube [https://git.duniter.org/HugoTrentesaux/jucube/](https://git.duniter.org/HugoTrentesaux/jucube/)

Un liste à jour de mes contributions est disponible sur le forum [https://forum.duniter.org/t/mes-contributions-a-la-g1-hugotrentesaux/8812](https://forum.duniter.org/t/mes-contributions-a-la-g1-hugotrentesaux/8812)