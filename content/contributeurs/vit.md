+++
title = "Vit"
description = "développeur de DuniterPy, travaille également sur Tikka"

[extra]
full_name = "Vincent Texier"
avatar = "vit.jpeg"
forum_duniter = "vit"
forum_ml = "vit"
g1_pubkey = "7F6oyFQywURCACWZZGtG97Girh9EL1kg2WBwftEZxDoJ"
website = "http://vit.free.fr/website/"
+++

Vit est développeur de DuniterPy et travaille également sur Tikka, un nouveau client spécialisé pour les commerces. A travaillé également sur Sakia (abandonné).