+++
title = "Elois"
description = "développeur principal de Duniter v2, il travaille également sur duniter-squid"

[extra]
avatar = "elois.jpg"
website = "https://librelois.fr/"
+++

Éloïs est le développeur principal de Duniter v2, il travaille également sur [duniter-squid](https://git.duniter.org/tools/duniter-squid).