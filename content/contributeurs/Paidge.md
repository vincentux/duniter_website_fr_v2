+++
title = "Paidge"
description = "développeur de la Wotmap"

[extra]
full_name = "Pierre-Jean Chancellier"
avatar = "Paidge.png"
forum_duniter = "paidge"
forum_ml = "paidge"

[taxonomies]
authors = ["Paidge",]
+++

Paidge est le développeur de la Wotmap [https://wotmap.duniter.org/](https://wotmap.duniter.org/).