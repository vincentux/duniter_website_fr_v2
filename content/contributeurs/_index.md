+++
title = "Contributeurs"
date = 2020-07-29
weight = 5
template = "custom/equipe.html"
page_template = "authors/page.html"
aliases = ["wiki/about/equipe", ]

[extra]
# contrôle la liste des contributeurs (nom de fichier)
authors = [
    "elois",
    "cgeek",
    "kimamila",
    "Moul",
    "1000i100",
    "vit",
    "Paidge",
    "gerard94",
    "HugoTrentesaux",
    "poka",
    "tuxmain",
    "manutopik",
    "matograine",
    "CaTasTrOOf",
    ]
+++

Nous listons ici les personnes ayant apporté une contribution technique au projet. Pour y apparaître, veuillez compléter [la page dédiée](https://forum.duniter.org/t/lequipe-qui-travaille-sur-quoi/9076) sur le forum, ou faire une MR sur [le dépôt](https://git.duniter.org/websites/duniter_website_fr_v2) du site.