+++
title = "cgeek"
description = "développeur original du logiciel Duniter v1"

[extra]
full_name = "Cédric Moreau"
avatar = "cgeek.png"
website = "https://blog.cgeek.fr/"
forum_duniter = "cgeek"
forum_ml = "cgeek"
+++

Cédric Moreau est le développeur original du logiciel Duniter et auteur principal du protocole.