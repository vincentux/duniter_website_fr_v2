+++
title = "poka"
description = "développeur de Ğecko et administrateur de nombreux outils"

[extra]
full_name = "Étienne Bouché"
avatar = "poka.svg"
forum_duniter = "poka"
forum_ml = "poka"
g1_pubkey = "Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P"

[taxonomies]
authors = ["poka",]
+++

Poka est administrateur système. Il gère une grande partie de l'infrastructure (forums, ...). Il développe activement Ğecko et est aussi l'auteur de Ğ1-stats et Jaklis.