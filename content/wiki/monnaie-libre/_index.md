+++
title = "Monnaie libre"
weight = 2

[extra]
katex = true
toc_depth = 2

[taxonomies]
tags = [ "pages-principales",]
+++

{% note(markdown=true, display="block", type="warning") %}
Le concept de monnaie libre est défini précisément par la [théorie relative de la monnaie](https://trm.creationmonetaire.info/) (TRM). Bien que la lecture de ce document soit conseillée pour une bonne compréhension, nous proposons ici un résumé des points principaux. Consultez la page [ressources](]@/wiki/monnaie-libre/ressources.md) pour une compréhension plus intuitive ou vulgarisée.
{% end %}

# Monnaie libre

[TOC]

## Introduction

La TRM repose sur l'idée que l'être humain est source et juge de toute valeur, et que toute valeur est donc relative à l'humain qui l'estime. Une monnaie, en tant qu'outil de mesure de valeur, n'existe donc que par l'humain et doit être créée en proportion de leur nombre. Combinée avec <span style="border-bottom: dashed 1px black;" title="Les hommes naissent et demeurent libres et égaux en droits. Les distinctions sociales ne peuvent être fondées que sur l'utilité commune.">l'article 1er</span> de la <a href="https://www.conseil-constitutionnel.fr/le-bloc-de-constitutionnalite/declaration-des-droits-de-l-homme-et-du-citoyen-de-1789">déclaration des droits de l'homme et du citoyen de 1789</a>, cette idée aboutit au principe dit *de symétrie* stipulant que la monnaie doit être créée en parts égales entre tous les individus. Une monnaie ne respectant pas ce principe est dite *asymétrique* et suppose implicitement l'existence d'une notion de valeur extérieure à l'humain, c'est-à-dire indépendante de tout jugement. Ce qui suit vise à formaliser ce raisonnement pour le concrétiser.

## Définitions et hypothèses

##### Zone monétaire

Une zone monétaire est l'espace social formé par un ensemble d'individus utilisant une monnaie commune. C'est le repère dans lequel est défini le concept de monnaie libre. Elle peut varier d'un point de vue démographique, par les entrées et sorties comme les naissances et décès. La TRM fait l'hypothèse d'une démographie de taille $N$ constante et d'une espérance de vie homogène.

##### Monnaie et masse monétaire

Une monnaie est un intermédiaire d'échange qui peut servir d'outil de mesure de valeur et de réserve de valeur. La quantité totale de monnaie à un instant $t$ est appelée masse monétaire et notée $M(t)$.

##### Principe de symétrie

Une monnaie respectant le principe de symétrie n'introduit pas d'inégalités entre les individus de la zone monétaire. C'est à la fois une symétrie spatiale et temporelle : aucun individu n'est privilégié vis-à-vis de la création monétaire par rapport à un autre individu contemporain ou futur. 

##### Unité relative et pouvoir

Une quantité donnée de monnaie n'a de sens que relativement à la masse monétaire. On appelle *pouvoir* (noté $P$) une proportion de la monnaie totale. Le pouvoir moyen vaut $P=M/N$.

##### Dividende universel

On appelle dividende universel (noté $\mathrm{DU}$) la monnaie créée en quantité égale par chaque individu de la zone monétaire pendant un intervalle de temps $\mathrm{d}t$.


## Théorème

> Si une monnaie respecte le principe de symétrie, alors elle est créée sous forme d'un dividende universel et la masse monétaire évolue suivant la loi exponentielle :

££M(t)=M(0)e^{ct}££


### Démonstration

Avec $m(x,t)$ la monnaie d'un individu $x$ (modèle continu) à l'instant $t$, la création monétaire pour l'individu vaut :
 ££ \mathrm{d}m = \frac{\partial m}{\partial x}\mathrm{d}x + \frac{\partial m}{\partial t}\mathrm{d}t ££

la symétrie spatiale (égalité entre les individus) implique :
 ££ \frac{\partial m}{\partial x} = 0 ££

donc $\mathrm{d}m$ ne dépend pas de $x$ et l'on peut exprimer la masse monétaire $M(t)$ dans la population $X$ :
 ££  M(t) = \int_{X} m(x,t) \mathrm{d}x ££

la symétrie temporelle (égalité entre les générations) implique: 
 ££ \frac{\mathrm{d} M}{\mathrm{d} t} = cM(t) ££

autrement dit, chaque génération créera une *proportion égale* ($c$) de la masse monétaire courante ($M$).

Les [solutions à cette équation](https://fr.wikipedia.org/wiki/%C3%89quation_diff%C3%A9rentielle_lin%C3%A9aire_d'ordre_un) sont de la forme :
 ££ M(t) = M(0)e^{ct} ££

$\blacksquare$

### Notes

#### Expression du DU
Il est intéressant de noter que l'expression du dividende universel est donc :

££ \text{DU} = c\frac{M}{N}\mathrm{d}t ££

#### Choix de c
Le théorème ne donne pas de valeur pour $c$. Ce choix a des conséquences qui sont discutées dans la page [choix de c](@/wiki/monnaie-libre/choix-de-c.md) et qui peuvent être débattues sur le forum monnaie libre.

### Corollaires

#### Convergence vers la moyenne
Quand un individu entre dans la zone économique (à sa naissance), il commence à créer de la monnaie. Notons $P(t_a)$ le pouvoir de la somme monnaie créée entre sa naissance $t_0 = 0$ (prise comme référence) et son âge $t_a$. Par définition du DU (création de monnaie d'un individu), on a :
 ££ P(t_a) = \int_0^{t_a} \text{DU}(t) ££
soit, en remplaçant le DU par sa valeur exprimée plus haut
 ££ P(t_a) = \int_0^{t_a} c\frac{M(t)}{N}\mathrm{d}t ££
ce qui après calcul (intégrale d'une exponentielle) donne

 ££ P(t_a) = \frac{M(t_a)}{N}(1-e^{-ct_a}) ££

On peut vérifier qu'à la naissance aucune monnaie n'a été créée (on a bien $P(0)=0$). Pour un temps infini, P tend asymptotiquement vers une proportion $1/N$ de la masse monétaire, définie comme le pouvoir moyen.

<div id="graphique">
  <div><canvas id="canvas"></canvas></div>
  <div><blockquote>
    <span>Ajuster la création monétaire annuelle :</span>
    <input id="rangeInput" type="range" value="5" step="0.1" min="0" max="20" oninput="amount.value=rangeInput.value" />
    <input id="amount" type="number" value="5" step="0.1" oninput="rangeInput.value=amount.value" />
    <span id="percentage_per_year">5</span><span>% par an</span>
  </blockquote></div>
</div>

<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    #rangeInput {
        background-color: #FFF;
    }
    #graphique {
    	display: block;
        width:100%;
    }
    #graphique > div {
    }
</style>

<script src="https://files.trentesaux.fr/extra/courbes_interactives/js/Chart.bundle.js"></script>
<script src="https://files.trentesaux.fr/extra/courbes_interactives/js/utils.js"></script>
<script src="https://files.trentesaux.fr/extra/courbes_interactives/js/graphique.js"></script>

#### Réduction progressive des inégalités
En ajoutant la même part à tout le monde, on réduit les inégalités. Par exemple, si dans un groupe de dix personnes, une personne a réussi à prendre toute la monnaie (100% pour lui 0% pour chaque autre), si l'on donne 10% à chacun, la répartition sera moins inégale (55% pour lui, 5% pour chaque autre), si l'on donne à nouveau 10% à chacun, on réduira encore les inégalités (40% pour lui, 6.6% pour chaque autre)...
Au bout d'un temps infini (s'il n'y a pas d'échanges) tout le monde se retrouvera à 10%. La convergence vers la moyenne est vraie quelle que soit la quantité de monnaie collectée par un individu.

#### Disparition de l'impact monétaire
Quand un individu sort de la zone économique (à son décès) il cesse de créer de la monnaie. Même s'il lui reste une quantité importante de monnaie "bloquée", cette part relative décroit avec le temps. En effet par définition un pouvoir $P_d$ vaut initialement $M_d/M(0)$. Après un temps $t_d$, le pouvoir vaut $P(t_d)=M_d/M(t_d)$ soit
 ££ P(t_d) = e^{-ct_d} ££
Même si la quantité absolue $M_d$ de monnaie bloquée ne varie pas, son pouvoir, égal à sa quantité relative, diminue exponentiellement. C'est aussi la raison pour laquelle il faut éviter de prendre une valeur de $c$ trop grande.


## Sources

- [https://trm.creationmonetaire.info/](https://trm.creationmonetaire.info/)
- [https://blog.trentesaux.fr/monnaie-egalitaire/](https://blog.trentesaux.fr/monnaie-egalitaire/)