+++
title = "Ressources"
+++


## Aller plus loin

* [Théorie Relative de la Monnaie](http://trm.creationmonetaire.info/), par Stéphane Laborde
* Approfondir la TRM :
    * [Le module Galilée](http://rml.creationmonetaire.info/modules/index.html)
    * [Le module Yoland Bresson](http://rml.creationmonetaire.info/modules/module_yoland_bresson.html)
    * [Le module Leibnitz](http://rml.creationmonetaire.info/modules/module_leibnitz.html)
* [Ğlibre](http://www.glibre.org/)

### Compréhension et vulgarisation

* [Questions fréquentes sur les monnaies libres](@/faq/monnaie-libre/_index.md)
* *[La TRM En Couleur](http://cuckooland.free.fr/LaTrmEnCouleur.html)* par cuckooland
* *[La TRM pour les enfants](http://cuckooland.free.fr/LaTrmPourLesEnfants.html)* par cuckooland
* *[La TRM en détails](http://monnaie.ploc.be/)* par Emmanuel Bultot
* Vidéo par [Vincent Texier aux RML7](https://www.youtube.com/watch?v=pSaPjxIpJGA&list=PLr7acQJbh5rzgkXOrCws2bELR8TNRIuv0&index=9) abordant les raisons d'une monnaie libre, à Laval en juin 2015
* Vidéo de [Stéphane Laborde à l'Université d'été du MFRB](https://www.youtube.com/watch?v=PdSEpQ8ZtY4) sur la monnaie libre, en été 2014
* Vidéo *[Le paradigme TRM et le RdB](https://www.youtube.com/watch?v=PdSEpQ8ZtY4)* par Stéphane Laborde
* Vidéo *[Révolution monétaire : débat entre Etienne Chouard, Stéphane Laborde et Jean-Baptiste Bersac](https://www.youtube.com/watch?v=kvjstlFaxUw)* par Le 4ème singe
* Vidéos *[Discussions sur la monnaie libre (Étienne Chouard / Stéphane Laborde)](https://www.youtube.com/playlist?list=PL9isKtkLy16dtuwbYGW_KXP99WRCo85pQ)* montées par Denis La Plume
* Vidéo de [Stéphane Laborde à l'Ubuntu Party](https://www.youtube.com/watch?v=ljflI-JAsbc) sur la monnaie libre, à Paris en novembre 2014
* *[« Monnaie de singe et Monnaie Libre » sur RADIOM ](https://www.radiom.fr/podcast/71-passeurs-de-bougnettes/3453-monnaie-de-singe-et-monnaie-libre.html)* par matiou et nadou
* *[« La Monnaie Libre au coeur des débats » sur RADIOM ](https://www.radiom.fr/podcast/71-passeurs-de-bougnettes/3621-la-monnaie-libre-au-coeur-des-debats.html)* par matiou et nadou
* *[Le livre « La monnaie : ce qu'on ignore » de Denis La Plume](https://blog.denislaplume.fr/2017/09/29/le-livre-la-monnaie-ce-quon-ignore-est-sorti/)*

### Articles 

* *[De l'intérêt d'une monnaie libre !](https://blog.cgeek.fr/de-linteret-dune-monnaie-libre.html)* par cgeek
* *[Duniter, Théorie relative de la monnaie et projets autour des monnaies libres](https://moul.re/blog/index.php?article3/duniter)* par Moul

### Blockchain

* [La blockchain : une solution technique pour la monnaie libre](@/faq/duniter/blockchain.md) par cgeek
* [Comment fonctionne Duniter](@/faq/duniter/fonctionnement.md) par cgeek
* [Le projet OpenUDC](https://github.com/Open-UDC/open-udc), précurseur de Duniter
* Vidéo *[Une monnaie libre avec OpenUDC/uCoin](https://www.youtube.com/watch?v=ljflI-JAsbc)* par Stéphane Laborde

