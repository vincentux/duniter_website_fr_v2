+++
title = "Doc Duniter"
weight = 1
sort_by = "weight"

aliases = ["fr/miner-des-blocs"]
+++

La gestion de la documentation Duniter est en cours de réorganisation. Vous trouverez ci-dessous la documentation en français pour les versions en cours. Pour les versions suivantes, la documentation sera gérée directement dans le dépôt Duniter et présentée sur ce site dans la section automatique [Duniter 1.9](@/wiki/doc/duniter1.9doc/_index.md). 


### Installation

*   [Installer son nœud Duniter](@/wiki/doc/installer/index.md)
*   [Mettre à jour son nœud Duniter](@/wiki/doc/mettre-a-jour.md)
*   [Mettre à jour Duniter sur YunoHost en ligne de commande](https://forum.duniter.org/t/full-https-support-for-duniter-package-for-yunohost/1892/18)

### Configuration

* [Configurer son nœud Duniter fraichement installé](@/wiki/doc/configurer.md)
* [Avoir plusieurs pairs partageant une même clé](@/wiki/doc/cles-partagees.md)
* [Ajouter/Retirer des interfaces spécifiques de pair](@/wiki/doc/interfaces-specifiques-de-pair.md)

### Utilisation

*   [Lancer Duniter automatiquement au démarrage de la machine](@/wiki/doc/lancement-au-boot.md)
*   [Duniter en ligne de Commande](@/wiki/doc/commandes.md)
*   [Les modules (plugins)](@/wiki/doc/modules.md)
*   [Liste des modules (plugins)](@/wiki/doc/modules.md)

### Auto-hébergement

*   [Duniter sur un VPN](https://forum.duniter.org/t/duniter-sur-un-vpn/2280/13)
*   [Duniter sur YunoHost derrière une box privatrice (type livebox)](https://forum.duniter.org/t/duniter-sur-yunohost-derriere-une-box-privatrice-type-livebox/2169)
*   [Duniter sur Raspberry Pi 3 derrière une Freebox v5 avec Yunohost](@/wiki/doc/raspberry-pi-freebox-yunohost.md)

### Rémunération des forgerons

*   [Comment être rémunéré en calculant des blocs](@/wiki/doc/remuneration-calcul-blocs.md)
