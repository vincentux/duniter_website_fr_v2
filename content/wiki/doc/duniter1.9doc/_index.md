+++
title = "[archive] Documentation pour Duniter 1.9"

[extra]
EXTERNAL_CONTENT = "https://git.duniter.org/nodes/typescript/duniter/-/raw/dev/doc/use/index.md"
auto_toc = true
+++

Title: User documentation for Duniter Server
Order: 9
Date: 2017-09-22
Slug: configurer
Authors: elois

# User documentation for Duniter Server

- [Use Duniter Docker image](@/wiki/doc/duniter1.9doc/docker.md)
- [Configure Duniter server variant](@/wiki/doc/duniter1.9doc/configure.md)
- [WS2P: preferred and privileged nodes](@/wiki/doc/duniter1.9doc/ws2p_preferred_privileged.md)
- [Compile Duniter manually from source code](@/wiki/doc/duniter1.9doc/manual_compilation.md)
- [Advanced commands](@/wiki/doc/duniter1.9doc/advanced-commands.md)
        
