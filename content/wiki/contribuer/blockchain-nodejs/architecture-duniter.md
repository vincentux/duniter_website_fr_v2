+++
title = "Architecture"
date = 2017-10-09
aliases = [ "fr/architecture-duniter",]
weight = 10

[taxonomies]
authors = [ "vit",]
+++

# Architecture

## Architecture réseau des clients

Voici un aperçu de l'architecture de Duniter entre les clients et un serveur.

![uml](/PELICAN/uml/00182988.svg)

