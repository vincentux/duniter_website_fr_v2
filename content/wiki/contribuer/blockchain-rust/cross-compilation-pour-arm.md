+++
aliases = [ "cross-compilation-pour-arm",]
date = 2018-06-09
weight = 9
title = "Cross-compiler Dunitrust pour arm"

[taxonomies]
authors = [ "elois",]
+++

# Cross-compiler Dunitrust pour arm

Il y a deux Méthodes différentes pour cross-compiler Dunistrust pour arm :

[TOC]

La 1ère méthode a l'inconvénient de ne pas bénéficier des optimisations  de l'architecture armv7. Elle est toutefois plus propre, c'est la méthode que j'adopterai pour les livrables officiels.  

La 2ème méthode à l'inconvénient de compiler Dunitrust sans OpenSSL, le noeud duniter qui en résulte ne pourra donc cas conatcter des endpoints qui exigent une couche TLS.

## Méthode 1 : Cross-compiler proprement pour armv6 via docker

Tout d'abord installez docker, pour cela suivez la documentation officielle :

Ensuite cloner le dépot Dunitrust puis placez vous a sa racine :

```bash
git clone https://git.duniter.org/nodes/rust/duniter-rs.git
cd duniter-rs
```

Enfin lancez l'image docker qui se chargera de compiler Dunistrust pour armv6 :

```bash
sudo docker run -it --rm -v $(pwd):/source dlecan/rust-crosscompiler-arm:stable
```

A noter que cette image docker permet de compiler n'importe quel programme rust, elle n'est pas spécifique a Duniter.  
Vous pouvez retrouver les sources de cette image et le manuel expliquant les différentes options sur le dépot github : https://github.com/dlecan/rust-crosscompiler-arm

Une fois la compilation terminée, votre binaire final est le fichier `durs` qui se trouve dans `target/arm-unknown-linux-gnueabihf/release`.  
C'est un "binaire tout en un" vous pouvez le copier n'importe ou sur votre raspberry pi puis vous n'avez plus qu'a l'éxécuter dans un terminal.

## Méthode 2 : Cross-compiler manuellement pour armv7

Fonctionne sur ubuntu 16.04.
Vous devez avoir rust d'installé sur votre machine, si tel n'est pas le cas installez Rust en une seule ligne de commande : 

```bash
curl https://sh.rustup.rs -sSf | sh
```

Une fois que vous avez Rust, ajoutez la toolchain arm :

```bash
rustup target add armv7-unknown-linux-gnueabihf
```

Installez les paquets debian suivants :

```bash
sudo apt-get install gcc-4.7-multilib-arm-linux-gnueabihf crossbuild-essential-armhf
```

Indiquez a cargo quel compilateur il doit utiliser lorsqu'il compilera avec la toolchain arm, pour cela créez le fichier `~/.cargo/config` et collez y le contenu suivant :

```bash
[target.armv7-unknown-linux-gnueabihf]
linker = "arm-linux-gnueabihf-gcc-4.7"
```

Rendez vous a la racine du dépot de Dunistrust puis compilez avec la toolchain arm :

```bash
cd duniter-rs
cargo build --target=armv7-unknown-linux-gnueabihf --no-default-features --release
```

Si votre terminal vous dit que la commande `cargo` n'existe pas, c'est que vous devez ajouter le chemin `~/.cargo/bin` à votre variable d'environnement `PATH` :

```bash
export PATH="$HOME/.cargo/bin:$PATH"
```

_Explication des options de la commande de compilation :_

* target : indique a cargo la toolchain avec laquelle compiler.
* no-default-features : indique a cargo de ne pas inclure les features optionelles, cela permet nottament de ne pas inclure OpenSSL qui est très difficile à cross-compiler. (OpenSSl est loin d'être indispensable, ça seule utilitée est de permettre a WS2P Privé de contacter des endpoints WS2P en https).  
* release :  Permet de compiler en mode release, c'est a dire avec les optimisations de code et sans les points d'entrée nécessaire à un déboggeur. SI vous omettez cette option, cargo compilera en mode debug, c'est adire sans optimisation de code et avec les points d'entrée permetant d'utiliser un déboggeur. La compilation en mode debug est plus rapide mais le binaire final est beaucoup plus gros et duniter beaucoup plus lent, a n'utiliser que pour débugger donc.

Votre binaire final est le fichier `durs` qui se trouve dans `target/armv7-unknown-linux-gnueabihf/release`. C'est un "binaire tout en un" vous pouvez le copier n'importe ou sur votre raspberry pi puis vous n'avez plus qu'a l'éxécuter dans un terminal.
