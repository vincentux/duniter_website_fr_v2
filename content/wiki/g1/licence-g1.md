+++
aliases = ["licence-g1", "fr/monnaie-libre-g1/licence-g1"]
date = 2017-10-04
weight = 9
title = "Licence Ğ1"

[taxonomies]
authors = [ "cgeek", "Galuel",]
+++

# Licence Ğ1

{% note(type="warning", size="large", markdown=true) %}**L'utilisation de la la monnaie Ğ1 suppose l'acceptation de sa licence.** 

[Télécharger la Licence Ğ1 (format texte)](https://git.duniter.org/documents/licence-g1/-/raw/master/license/license_g1-fr-FR.rst)
ou
[Consulter sur ce site (page web)](/wiki/g1/licence-txt/)
{% end %}

## Commentaire

{% note(type="info", display="block", markdown=true) %}

Une fois la licence reçue, lue et acceptée, le processus pour devenir membre de la TdC Ğ1 passe par la création d'un ID (votre identifiant / pseudo) et d'une paire (clé privée / clé publique) [de façon sécurisée](https://forum.duniter.org/t/recommandations-de-securite-a-lire-avant-de-creer-votre-compte-g1/1829). La clé publique devient un numéro de compte utilisable uniquement par celui qui possède la clé privée, laquelle se construit par une autre paire (phrase secrète / password) = clé privée, qu'il faut précieusement garder pour soi.

Le plus efficace pour réaliser cela consiste à utiliser l'un des clients Duniter Ğ1 officiels :

* Silkaj [https://silkaj.duniter.org/](https://silkaj.duniter.org/)
* Sakia [http://sakia-wallet.org/](http://sakia-wallet.org/)
* Cesium [https://cesium.app/fr/](https://cesium.app/fr/)

Pour des raisons de sécurité il sera préférable de générer votre paire de clés en étant offline et il est conseillé de ne jamais saisir votre clé privée membre (qui est la source de votre Dividende Universel Ğ1) sur une application connectée ou un site web externe (Cesium qui apparaît sous la forme de pages web peut parfaitement être installé et utilisé sur votre propre PC, il est d'ailleurs embarqué nativement avec le serveur Duniter dans ce but, proposant ainsi une application 100% client / serveur).

Une fois votre paire de clés créée, vous devez générer un document Duniter de révocation, qui vous assure de pouvoir annuler votre compte en cas de problème futur que vous devrez garder précieusement.

Ensuite vous générez un document Duniter de demande pour devenir membre, lequel devra être certifié par des personnes qui vous connaissent suffisamment bien (voir impérativement la licence de la monnaie Ğ1 à ce propos) et qui sont déjà membres de la TdC Ğ1. Il est donc plus efficace de s'assurer préalablement que au moins 5 membres de la TdC Ğ1 vous connaissent suffisamment bien et accepteront ainsi de certifier votre clé publique, avant de commencer le processus d'inscription.

Il faudra 5 certifications pour que votre inscription puisse être inscrite dans la blockchain Ğ1. Toutefois à cause des règles de temps (1 signature par personne tous les 5 jours), et de calcul (calculs des blocs par le réseau Duniter Ğ1) cette opération finale pourra prendre du temps (plusieurs jours, voire plusieurs semaines selon les cas). Si votre opération n'est pas validée au bout de 2 mois il faudra la recommencer.

Note : Duniter vous permet de générer autant de comptes non-membres que voulu. Un conseil de gestion est donc de faire de petits virements sur ces comptes d'appoint (ou portes-monnaie), possédant leur propre clé privée, ce qui limitera les risques d'utilisation à la quantité de monnaie Ğ1 qui s'y trouvera. Vous pourrez ainsi utiliser ce porte monnaie sur des applications online en lesquelles vous avez confiance tout en limitant les risques associés à la connexion.
{% end %}

## Suivi de l'inscription

Une fois le processus d'inscription démarré, il est possible de suivre son avancement via deux outils :

* [g1-monit](https://g1-monit.librelois.fr/willMembers?lg=fr) : visualisation de l'état des inscriptions en attente
* [WotWizard](https://wot-wizard.duniter.org) : prévisions des dates d'entrée de chaque inscription

La combinaison des ces deux outils suffit à savoir si votre dossier d'inscription est complet et valide, ainsi que de connaître votre date d'entrée probable dans la toile de confiance.

À noter que seuls les dossiers complets sont répertoriés par WotWizard.

