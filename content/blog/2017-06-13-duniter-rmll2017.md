+++
aliases = [ "duniter-rmll2017",]
date = 2017-06-13
title = "Duniter aux RMLL 2017 de Saint-Étiennexa0!"

[extra]
thumbnail = "/PELICAN/images/calendar.svg"

[taxonomies]
authors = [ "cgeek",]
tags = [ "RMLL2017",]
category = [ "Évènements",]
+++

# Duniter aux RMLL 2017 de Saint-Étienne !

[![Bannière RMLL 2017](/PELICAN/images/rmll2017.png)](https://2017.rmll.info/)

Les Rencontres Mondiales du Logiciel Libre (RMLL) édition 2017 se dérouleront à Saint-Étienne du 1er au 7 Juillet 2017.

Deux développeurs de l'équipe Duniter seront dépêchés sur place pour vous parler monnaie libre, Ğ1 et de son écosystème logiciel ! Nous vous proposons **2 conférences le jeudi 6 juillet**.

### Conférence #1

#### « Si l'échange est libre, où est la monnaie libre ? »

La conférence aura lieu *le Jeudi 6 Juillet 2017 à 09h20*, salle J106. Durée : 20 min.

Intervenant : cgeek

[https://prog2017.rmll.info/programme/partage-aspects-collectifs-du-libre/si-l-echange-est-libre-ou-est-la-monnaie-libre](https://prog2017.rmll.info/programme/partage-aspects-collectifs-du-libre/si-l-echange-est-libre-ou-est-la-monnaie-libre)

### Conférence #2

#### « Une monnaie libre pour une société libre »

La conférence aura lieu *le Jeudi 6 Juillet 2017 à 14h00*, salle J106. Durée : 40 min.

Intervenant : Moul

[https://prog2017.rmll.info/programme/partage-aspects-collectifs-du-libre/une-monnaie-libre-pour-une-societe-libre](https://prog2017.rmll.info/programme/partage-aspects-collectifs-du-libre/une-monnaie-libre-pour-une-societe-libre)

### Notes

Il est très probable que la salle J106 soit une salle de classe, comprenant donc une vingtaine de places.

