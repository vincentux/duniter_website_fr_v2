+++
description = "Un Ğeconomicus est organisé à Lodève le 18/06/2017."
aliases = [ "geconomicus-lodeve-18-06-2017",]
date = 2017-06-14
title = "Ğeconomicus à Lodève le 18/06/2017"

[extra]
thumbnail = "/PELICAN/images/geconomicus.png"

[taxonomies]
authors = [ "cgeek",]
tags = [ "Ğeconomicus", "jeu", "monnaie libre",]
category = [ "Évènements",]
+++

# Ğeconomicus à Lodève le 18/06/2017

Un Ğeconomicus est organisé à Lodève le 18/06/2017.

[![Ğeconomicus à la Compagnie des Jeux, de 10h à 18h](/PELICAN/images/evenements/geconomicus-lodeve.jpg)](https://www.monnaielibreoccitanie.org/event/jeu-geconomicus-a-lodeve/)

Plus d'informations sur [monnaielibreoccitanie.org](https://www.monnaielibreoccitanie.org/event/jeu-geconomicus-a-lodeve/)

