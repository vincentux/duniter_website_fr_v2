+++
aliases = [ "refonte-du-site",]
updated = 2017-04-03
date = 2017-04-03
title = "Refonte du site /fr !"

[extra]
thumbnail = "/PELICAN/images/www.svg"

[taxonomies]
authors = [ "cgeek",]
tags = [ "site",]
category = [ "Communication",]
+++

# Refonte du site /fr !

Certains auront pu le remarquer depuis hier après-midi : la version française du site duniter.org a changé de look !

En réalité, cela va plus loin qu'un simple changement ergonomique. Petit tour du propriétaire.

## Une nouvelle ergonomie

D'abord, l'ergonomie a été revue : nous souhaitions que le site soit plus attrayant et ne freine pas sa consultation. Quelques images supplémentaires et un agencement revu évitent un accueil trop austère :

![Le site de duniter : avant et après](/PELICAN/images/refonte/changement.png)

## Un site entièrement éditable

Une des grandes nouveautés : le site est désormais [entièrement éditable](@/wiki/contribuer/ameliorer-le-site.md) !

Vous trouverez sur chacune des pages dans la barre latérale un bouton « Modifier la page sur GitHub » ou « Modifier l'article sur GitHub ». Il ressemble à ceci :

![Bouton "Modifier la page sur GitHub"](/PELICAN/images/wiki/btn_modifier.png)

Vous pourrez donc proposer des modifications ou même ajouter du nouveau contenu à ce site. Une simple validation de notre part, et voilà le site modifié !

En espérant que cette nouvelle version vous plaise !


