+++
title = "Titre"
date = 2000-01-01
draft = true

[taxonomies]
authors = ["Moi",]
tags = ["demo",]

[extra]
thumbnail = "/img/yunohost.png"
+++

Contenu

{% note(type="ok", display="block", markdown=true) %}
Note "ok" *échantillon*. De type "block"  
Avec du contenu en **markdown**.
{% end %}

{% note(type="ok") %}
Note both "ok" sur une seule ligne.
{% end %}

{% note(type="ok", display="icon", markdown=true, size="large") %}
Note "ok" *échantillon*. De type "icon"  
Avec du contenu en **markdown**.
{% end %}

{% note(type="ok", display="icon") %}
Note icone "ok" sur une seule ligne.
{% end %}

{% note(type="warning", display="block", markdown=true) %}
Note "warning" *échantillon*. De type "block"  
Avec du contenu en **markdown**.
{% end %}

{% note(type="warning") %}
Note both "warning" sur une seule ligne.
{% end %}

{% note(type="warning", display="icon", markdown=true, size="large") %}
Note "warning" *échantillon*. De type "icon"  
Avec du contenu en **markdown**.
{% end %}

{% note(type="warning", display="icon") %}
Note icone "warning" sur une seule ligne.
{% end %}

