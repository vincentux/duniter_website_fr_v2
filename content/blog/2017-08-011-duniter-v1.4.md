+++
aliases = [ "duniter-version-1.4",]
date = 2017-08-11
title = "Duniter version 1.4"

[extra]
thumbnail = "/PELICAN/images/box.svg"

[taxonomies]
authors = [ "cgeek",]
tags = [ "release",]
category = [ "Moteur blockchain",]
+++

# Duniter version 1.4

La version 1.4 de Duniter est désormais [disponible au téléchargement](https://github.com/duniter/duniter/releases/tag/v1.4.15) !

## Changements

Cette version apporte plusieurs améliorations de stabilité du réseau, notamment à travers une résolution plus rapide des embranchements (*forks*) de la chaîne de blocs. Aussi, les identités et certifications en piscine sont désormais plus régulièrement resynchronisés.

Mais le changement majeur de cette version est **la migration du code source JavaScript vers TypeScript**.

Pour plus de détails sur tous ces changments, consulter [les notes de version](https://github.com/duniter/duniter/releases/tag/v1.4.13).

<span class="center-content"><img class="screenshot" src="/PELICAN/images/duniter-1.4/typescript.png" width="32" /></span>

## Synchronisation

{% note(type="ok", display="icon") %}Pas besoin de resynchroniser.{% end %}

## Compatibilité

{% note(type="ok", display="icon") %}Compatible avec la Ğ1.{% end %}

-----

## Mettre à jour sa version

* Lien pour [installer la nouvelle version](https://github.com/duniter/duniter/blob/master/doc/install-a-node.md) depuis un poste vierge
* Lien pour [mettre à jour vers la nouvelle version](https://github.com/duniter/duniter/blob/master/doc/update-a-node.md) depuis une installation existante

