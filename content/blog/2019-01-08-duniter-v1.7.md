+++
description = "La version 1.7 de Duniter vient de sortir. Elle apporte son lot de nouveautés, comme la possibilité de renouveller les certifications par anticipation, mais aussi une synchro plus rapide, et des performances accrues."
aliases = [ "fr/duniter-version-1.7",]
date = 2019-01-08
title = "Duniter version 1.7"

[extra]
thumbnail = "/PELICAN/images/box.svg"

[taxonomies]
authors = [ "cgeek",]
tags = [ "release",]
category = [ "Moteur blockchain",]
+++

# Duniter version 1.7

La version 1.7 de Duniter est désormais [disponible au téléchargement](https://git.duniter.org/nodes/typescript/duniter/-/releases) !

## Arrivée du renouvellement de certifications

Cette version est une avancée majeure dans la stabilité long terme de la Ğ1, puisque **cette version de Duniter active le protocole v11** qui autorise le renouvellemet anticipé des certifications !

### Qu'est-ce que ça apporte ?

Concrètement, en l'état actuel de la Ğ1 en protocole v10, si vous avez émis une certification envers quelqu'un alors vous devez attendre que celle-ci expire au bout de 2 ans avant de pouvoir la rejouer.

Cette règle est dommageable car elle empêche de certifier en continu la même personne, que pourtant l'on connaît bien. Au bout de 2 ans, cette ancienne règle peut créer des situations où le certifié perd son statut de membre temporairement, et donc ne produit plus de Ğ1 et ne peut plus certifier lui-même.

Mais grâce au protocole v11, il devient désormais possible de renouveller une certification *dès 2 mois après son émission*. Ainsi, si vous certifiez une personne le 1er janvier 2019, vous pourrez reconduire cette certification dès le 1er mars 2019. Cette reconduction repoussera d'autant (2 mois dans ce cas) la durée de validité de la certification initiale.

Si vous renouvelez au bout de 6 mois, la reconduction sera de 6 mois, etc.

### Comment activer le renouvellement ?

Actuellement, la Ğ1 est encore en protocole v10. Actuellement, près de 50% des membres calculants du réseau ont déjà mis à jour leur nœud par anticipation : à 60%, le réseau basculera la Ğ1 en protocole v11 !

Il ne reste donc plus que la participation de quelques membres pour officialiser cette bascule !

Pour cela, il vous suffit donc de mettre à jour votre nœud <= v1.6.27 en une version plus récente :

* soit en [version 1.7.9](https://git.duniter.org/nodes/typescript/duniter/tags/v1.7.9), compatible avec le protocole v11, apporte aussi quelques nouveautés !
* soit en [version 1.6.29](https://git.duniter.org/nodes/typescript/duniter/tags/v1.6.29), ancienne version rendue aussi compatible avec le protocole v11 

> Attention ! Si vous optez pour la version 1.7, **vous devrez resynchroniser votre nœud**. En effet, la structure de donnée a changé. Duniter ne verra plus aucune donnée au premier démarrage en 1.7.

## Changements

La version 1.7 de Duniter apporte aussi d'autres améliorations, notamment :

### Synchro plus rapide

Essayez ! Une machine correcte synchronise en moins de 10 minutes.

En 1.6 un Raspberry PI 3 pouvait prendre une dizaine d'heure à se synchroniser sur la Ğ1, il ne met plus que 30 minutes en 1.7.

### Performances accrues

Du reste, Duniter 1.7 améliore ses performances afin de **gérer la montée en puissance de la Ğ1 qui gère désormais plus de 1500 membres**.

Pour rappel, quelques informations de volumétrie de la Ğ1 au 24/11/2018 :

* 1,582 devenus membres
* 13,719 certifications
* 354,236 sources de monnaie
* 15,211 transactions

### Amélioration de la qualité

En interne, le logiciel connaît des refontes, abstractions, élagage de code afin de s'alléger mais surtout de renforcer sa qualité et s'assurer de son bon fonctionnement de version en version.

Également, ces améliorations préparent le terrain pour l'arrivée d'une nouvelle interface graphique, bien que celle-ci n'en soit encore qu'au stade de prototype.

## Synchronisation

{% note(type="warning", display="icon") %}Resynchronisation obligatoire.{% end %}


## Compatibilité

{% note(type="warning", display="icon") %}Fait évoluer la Ğ1 en protocole v11 !{% end %}

-----

## Mettre à jour sa version

* Lien pour [installer la nouvelle version](@/wiki/doc/installer/index.md) depuis un poste vierge
* Lien pour [mettre à jour vers la nouvelle version](@/wiki/doc/mettre-a-jour.md) depuis une installation existante

