+++
title = "Duniter version 1.8"
description = "La version 1.8 de Duniter vient de sortir. C'est la première version à intégrer des crates Rust utilisées par nodejs."
date = 2020-06-12

[extra]
thumbnail = "/PELICAN/images/box.svg"

[taxonomies]
authors = [ "elois",]
tags = [ "release",]
category = [ "Moteur blockchain",]
+++

# Duniter version 1.8

{% note(type="warning", display="both", markdown=true) %}
Cet article est une copie du sujet [publié sur le forum](https://forum.duniter.org/t/nouvelle-version-stable-de-duniter-v1-8-0/7377).
{% end %}


## Télécharger Duniter 1.8 variante [Server](https://git.duniter.org/nodes/typescript/duniter/-/releases/v1.8.0) ou [Desktop](https://git.duniter.org/nodes/typescript/duniter/-/releases/v1.8.1)

*L'image docker est disponible sur dockerhub : [duniter/duniter:v1.8.0](https://hub.docker.com/layers/duniter/duniter/v1.8.0/images/sha256-002858836321149dce40598b81425a6aa5120bbfbc879a72152b7841f9741a60)*

{% note(type="warning", display="block", markdown=true) %}
**Cette version nécessite une réinitialisation manuelle de la blockchain**
{% end %}

## Procédure de mise à jour

1. Télécharger et installer la nouvelle version
2. Stopper Duniter
3. Exécuter les commandes suivantes : 

    rm -rf ~/.config/duniter/duniter_default/data/
    rm ~/.config/duniter/duniter_default/wotb.bin

4. Selon votre variante :
  a. Desktop: relancer Duniter, vous serez alors obligé de refaire une synchronisation.
  b: Server: refaire une synchronisation (duniter sync HOST:PORT).

*Remplacez HOST et PORT par le nom d'hôte et le port du nœud de confiance sur lequel vous souhaitez vous synchroniser.*

## Dois-je installer cette version ?

Oui, elle corrige de nombreuses vulnérabilités  de sécurité et apporte une amélioration sensible des performances de la preuve de travail (mécanisme de forgeage des blocs).

@Blacksmith **tous les membres forgerons sont invités à se mettre à jour dès que possible.**

## Quels changements ?

Cette nouvelle version majeure de Duniter cumule beaucoup de petites modifications, dont certaines ont été réalisées il y a plus d'un an déjà (notamment le passage à nodejs v10); elle marque également le commencement d'une [**migration progressive de Duniter en Rust**](https://forum.duniter.org/t/duniteroxyde-oxydation-de-duniter/7075) !

Changements principaux : 

- Migration sous nodejs v10 @cgeek et @Moul  
- Migration en Rust de module wotb @elois
- Migration en Rust de toutes les fonctionnalités de cryptographie (sauf scrypt) @elois
- Remplacement vielles dépendances qui présentaient des vulnérabilités de sécurité @elois
- Auto-complétion bash  @vit
-  Amélioration du process de build et allègement des paquets @sveyret 

Cette version inclut de très nombreux autres petits changements, je vous invite à lire le CHANGELOG pour plus de détails : 

[https://git.duniter.org/nodes/typescript/duniter/-/blob/dev/CHANGELOG.md#v180-12th-march-2020](https://git.duniter.org/nodes/typescript/duniter/-/blob/dev/CHANGELOG.md#v180-12th-march-2020)