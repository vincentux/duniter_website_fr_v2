+++
title = "Migration du site francophone de Duniter"
description = "Brève histoire du site de duniter et annonce de la migration"
date = 2020-07-26

[taxonomies]
authors = ["HugoTrentesaux",]
tags = ["site", ]
category = [ "Communication",]

[extra]
thumbnail = "/PELICAN/images/www.svg"
+++

# Migration du site duniter.org

Le site de duniter a connu plusieurs évolutions successives et garde aujourd'hui une trace de cette longue histoire. Tout d'abord, il s'agissait de OpenUDC et Ucoin (**openudc.org** et **ucoin.org**). Les articles les plus anciens (2015-2016) témoingnent de cette époque.

En avril 2016 [ucoin devient duniter](@/blog/2016-04-24-ucoin-rename-duniter.md) et [participe aux rml7](@/blog/2016-05-10-duniter-sera-aux-rml7.md). La monnaie ğ1 est officiellement lancée le [8 mars 2017](@/blog/2017-03-08-g1-go.md) et le site connait [une refonte](@/blog/2017-04-03-refonte-du-site.md) dans la foulée en Pelican.

Il est traduit en anglais le [19 juin 2017](@/blog/2017-06-19-duniter-org-en.md) en tant que site indépendant afin de toucher un public anglophone.

La version francophone du site fait peau neuve en [décembre 2019](https://forum.duniter.org/t/refonte-site-web-en-francais-de-duniter/6662) grâce aux talents de webdesign de [Boris](https://borispaing.fr/). Il en ressort des pages d'accueil bien plus lisibles et "grand public".

Aujourd'hui je (Hugo Trentesaux) me suis proposé pour prendre la relève du site qui n'avait plus de mainteneur. Le site originel était en effet devenu difficile à maintenir et fonctionnait avec un version de Pelican très ancienne. J'ai donc réalisé une migration en Zola qui m'a donné l'occasion de ré-écrire un thème plus adapté tout en conservant les améliorations en matière de design.

![screenshot](/blog/screenshot_duniter.org_2020-07.png)

> *Capture d'écran du nouveau thème montant un menu déroulant facilitant la navigation*

La palette de couleur du site est choisie pour rappeler les couleurs du logo de duniter. Une barre de navigation facilite la navigation dans le site. Certaines fonctionnalités cassées sont revues et une [feuille de route](https://forum.duniter.org/t/site-web-de-duniter/7482/2) est adoptée.