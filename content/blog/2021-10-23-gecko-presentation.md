+++
title = "Présentation de Ğecko"
description = "Le client mobile Ğecko est en cours de construction, voici quelques nouvelles sur le projet."
date = 2021-10-23

[taxonomies]
authors = ["HugoTrentesaux",]
tags = ["logiciel", ]
category = [ "Technique",]

[extra]
thumbnail = "/img/smartphone.svg"
+++

## Contexte actuel du projet Duniter

Le projet Duniter est entré dans une phase un peu particulière : alors que la toile de confiance continue de grandir (3594 membres à l'heure où j'écris ces mots), et la blockchain de s'allonger (468661 blocs), l'équipe Duniter réflechit au futur de la monnaie. Les perspectives d'amélioration côté serveur apportées par l'[oxydation de duniter](@/blog/2020-12-17-oxydation-duniter.md) sont contrastées : d'une part, les larges gains en efficacité observés nous encouragent à continuer ce chantier, d'autre part, sa difficulté et le nombre très restreint de personnes actuellement capables d'y contribuer nous font douter de sa faisabilité.

À bien y réfléchir, une _refonte du protocole_ ainsi qu'une _ré-écriture totale du logiciel_ seraient préférables au long terme. En effet, même en menant l'oxydation jusqu'au bout (~4-5 ans de travail au rythme actuel), il resterait de profonds problèmes structurels dans le protocole qui menacerait le passage à l'échelle. Un ré-écriture serait quant à elle l'occasion de mettre à profit l'expérience engrangée sur les quatre premières années de fonctionnement de la monnaie sans chercher à se mettre en conformité avec des cas limites du protocole actuel. De plus, et se basant sur les progrès fait dans les dix dernières années dans le milieu de la blockchain, la durée de développement pourrait être réduite à ~2-3 ans. Nous aborderons ce sujet en détail dans un prochain article.

Il nous reste donc encore deux ans pour continuer à faire grandir la communauté Ğ1 et développer ses outils. C'est là qu'intervient le projet Ğecko, le premier d'une nouvelle génération de clients.

## Objectifs de Ğecko

Nous avons eu l'idée de Ğecko en observant la difficulté à réaliser des transactions en Ğ1 lors d'événements comme les Ğmarchés. En effet, l'application Césium est lente pour plusieurs raisons :
- nécessité de rentrer le mot de passe complet pour s'identifier
- technologie web (ionic) gourmande en ressources (mémoire, CPU)
- interface peu réactive car reposant sur une API vieillissante (BMA)
- risque de panne du noeud duniter / cesium+ sélectionné (client mono noeud)

Césium reste un bon client pour gérer son compte membre depuis un ordinateur, mais nous avons ressenti le besoin de construire un nouvel outil pour répondre à ces cas d'usage.

### Gestion des clés

Nous nous sommes aperçu que laisser le choix du mot de passe à l'utilisateur était une mauvaise idée, d'une part pour les oublis et d'autre part pour la faible sécurité. La méthode diceware recommandée restait compliquée à mettre en oeuvre pour l'utilisateur et peu appliquée. Pour Ğecko, nous avons choisi une nouvelle méthode d'authentification qui est à la fois plus robuste et plus simple d'utilisation.
La clé privée est générée aléatoirement et stockée dans un fichier protégé par un code à six lettres aléatoire. En cas de perte du téléphone, l'utilisateur peut regénérer sa clé privée à l'aide d'une phrase de restauration composée de 12 mots aléatoires dans la langue choisie.

De plus, en utilisant les techniques de HDWallet (Hierarchical Deterministic Wallet), nous pouvons dériver plusieurs portefeuilles d'une même clé privée, ce qui signifie un seul mot de passe à retenir, et une seule phrase de restauration à noter pour plusieurs comptes différents (par ex pro/perso).

{{ peertube(embed="https://tube.p2p.legal/videos/embed/098d6bdf-2684-41be-b290-10bdf6f15bd4") }}

> explication de la phrase de restauration et du mot de passe

### Technologie native

Pour atteindre de bonnes performances pour une faible utilisation de ressources machine, nous avons choisi le framework Flutter, qui permet de compiler le code de l'application pour iOS, Android, et bureau tout en incluant des bibliothèques précompilées. Par exemple, la gestion de la cryptographie sera assurée par une bibliothèque Rust à part, réutilisée dans d'autres clients. Cela permettra de partager les efforts de développements avec d'autres projets.

### Nouvelle API client

Ğecko se base sur GVA, la nouvelle API client des noeuds Duniter introduite dans l'[article précédent](@/blog/2020-12-17-oxydation-duniter.md#gva). Cela permet de faire des requêtes HTTP uniques comportant toutes les données nécessaires plutôt que plusieurs requêtes distinctes comportant beaucoup d'information superflue. Cela permet de diminuer largement le traffic réseau et de ramener la latence en dessous du seuil perceptible, même sur des connexions réseau modestes (3G). Nous essayons d'imaginer des protocoles hors ligne, mais n'avons pas encore de schéma en tête.

Nous travaillons également sur une bibliothèque client multi-noeud qui permettrait de s'affranchir des problèmes de noeud hors ligne ou désynchronisé.

## Moyens mis en oeuvre

Ğecko existe déjà à l'état de preuve de concept fonctionnelle, mais nécessite beaucoup d'heures de travail pour être amené à l'état de production. Il nous reste à compléter l'interface (restauration de compte, écran de paiement, gestion de portefeuilles, affichage de l'historique, confirmation de paiement...) mais également à amener Duniter 1.9 en production (étendre le périmètre de GVA, tester sa cohérence avec l'actuel, tester la robustesse...). Pour cela, nous avons cherché des financements en euro pour salarier nos développeurs, qui pourront ainsi consacrer bien plus de temps au projet, en comparaison du pur bénévolat.