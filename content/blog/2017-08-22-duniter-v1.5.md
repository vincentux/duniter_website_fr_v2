+++
aliases = [ "duniter-version-1.5",]
date = 2017-08-22
title = "Duniter version 1.5"

[extra]
thumbnail = "/PELICAN/images/box.svg"

[taxonomies]
authors = [ "cgeek",]
tags = [ "release",]
category = [ "Moteur blockchain",]
+++

# Duniter version 1.5

La version 1.5 de Duniter est désormais [disponible au téléchargement](https://github.com/duniter/duniter/releases/tag/v1.5.4) !

## Changements

Cette nouvelle version apporte principalement un lot de correctifs importants pour la stabilité de la monnaie :

* les nœuds sous architecture ARM fonctionnaient anormalement (puissance CPU divisée par 2 à chaque preuve de travail, désynchronisations intempestives)
* un nœud pouvait facilement s'isoler dans un fork
* les nœuds en version 1.4 étaient indiqués en gris dans Sakia (nœuds en erreur)
* Duniter renvoyait des informations erronnées à Cesium+

**Il est important de passer cette mise à jour** si vous êtes en version 1.3 ou inférieure, car ces anciennes versions comportent une ancienne version du protocole et provoquent régulièrement des forks sur le réseau. Bientôt, ces nœuds vont d'ailleurs s'isoler et ne seront plus d'aucune utilité.

## Synchronisation

{% note(type="ok", display="icon") %}Pas besoin de resynchroniser.{% end %}

## Compatibilité

{% note(type="ok", display="icon") %}Compatible avec la Ğ1.{% end %}

-----

## Mettre à jour sa version

* Lien pour [installer la nouvelle version](@/wiki/doc/installer/index.md) depuis un poste vierge
* Lien pour [mettre à jour vers la nouvelle version](@/wiki/doc/mettre-a-jour.md) depuis une installation existante

