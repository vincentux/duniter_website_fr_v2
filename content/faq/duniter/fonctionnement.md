+++
title = "Comment fonctionne Duniter ?"
aliases = [ "fr/fonctionnement",]
date = 2017-03-27
weight = 3

[taxonomies]
authors = [ "cgeek",]
+++

# Comment fonctionne Duniter ?

[TOC]

## La création de monnaie

Duniter est un *logiciel*. Son but est de permettre la production d'une nouvelle valeur numérique (ou électronique) ayant toutes les caractéristiques d'une [monnaie libre](@/wiki/monnaie-libre/_index.md) ». 

Nom de code de cette nouvelle valeur : la **Ğ1**.

La Ğ1 est désormais bien réelle : les toutes premières unités ont été créées le 8 mars 2017, et de nouvelles sont produites chaque jour par ses membres.

![De l'utilisateur à la création de monnaie](/PELICAN/images/comprendre/workflow.png)

## Le code de la monnaie

### Qui produit la monnaie

**Tout membre est co-producteur de la monnaie Ğ1** à travers le *Dividende Universel*.

Le dividende universel est une création de monnaie à parts égales pour tous les membres, à la fois dans l'espace et dans le temps.

Lorsqu'un Dividende Universel doit se produire, chaque membre de la monnaie produit automatiquement grâce au logiciel Duniter sa propre part de monnaie nouvelle. La seule action requise pour bénéficier de ce droit de production via Dividende Universel est de devenir *membre de la Toile de confiance*.

Dans Ğ1, le Dividende Universel est **la seule et unique façon de créer de la monnaie**.

### Comment sont identifiés les utilisateurs

**Tout membre doit être reconnu par la toile de confiance Ğ1** pour en faire partie.

Ainsi, tout individu qui souhaite intégrer la toile de confiance Ğ1 devra produire, à l'aide de logiciels, une identité qui devra être *reconnue* par des membres existants de la toile de confiance.

Cet acte de reconnaissance est appelé *certification*.

### Un réseau décentralisé

**La monnaie Ğ1 est décentralisée**, elle repose sur un enregistrement blockchain sur un réseau pair-à-pair.

L'ensemble des données de la monnaie Ğ1 est enregistré au sein d'une *blockchain*. Ainsi, la toile de confiance composée des identités membres et des certifications, ainsi que les transactions de monnaie sont enregistrées au sein d'une base de donnée partagée, répliquée, et écrite de façon décentralisée.

Par ailleurs, la blockchain Duniter [n'incitant pas à la course à la puissance de calcul](@/faq/duniter/duniter-est-il-energivore.md), on peut dire qu'elle est relativement « écologique ».

