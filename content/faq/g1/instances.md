+++
title = "Choisir une instance"
date = 2021-11-06
weight = 1

[taxonomies]
authors = [ "HugoTrentesaux",]
tags = []
+++

# Un modèle décentralisé

L'écosystème de la June est construit sur un modèle décentralisé, ce qui permet une gouvernance plus libre et une meilleure résilience. Cette diversité peut être un peu déstabilisante au début, mais cette page est là pour vous guider.

## Liste d'instances et implications

### Duniter

TODO expliquer les implications du choix d'une instance Duniter dans les clients mono-noeuds  
TODO mettre un lien sur comment changer son noeud Duniter dans les différents clients mono-noeuds  
TODO expliquer où trouver une liste des instances Duniter

### Césium+

TODO expliquer comment fonctionne Césium+ (notifications par exemple)  
TODO expliquer comment changer de noeud Césium+

- **g1.data.presles.fr**
- **g1.data.e-is.pro**
- **g1.data.adn.life**

### Ğchange+

Ğchange+ est un Datapod qui partage beaucoup de similarités avec Césium+.  
TODO expliquer davantage

- **data.gchange.fr**
- **gchange.data.presles.fr**

### WotWizard

WotWizard est une sorte d'appendice d'un noeud Duniter. Les données qu'il présente sont donc celles présentes sur le noeud Duniter associé. La partie prédictive de WotWizard concerne ce qui n'est pas encore écrit en blockchain, chaque instance a donc un point de vue différent.

- [https://wot-wizard.duniter.org/](https://wot-wizard.duniter.org/) -> noeud privé
- [https://wotwizard.coinduf.eu/](https://wotwizard.coinduf.eu/) -> duniter.coinduf.eu

### Ğ1monit

Ğ1monit est aussi un module Duniter, chaque instance présente les données de son noeud. Si ces données ne sont pas à jour, c'est sûrement que le noeud est désynchronisé par rapport au reste de la blockchain.

- [https://monit.g1.nordstrom.duniter.org/](https://monit.g1.nordstrom.duniter.org/) -> noeud privé

### TODO wotmap / worldwotmap / carte monnaie libre / g1-stats