+++
title = "FAQ"
weight = 3


# TODO unifier les faqs monnaie libre / Ğ1 / Duniter
# (l'idée d'une FAQ est que justement on ne sait pas forcément différencier les trois)
# réfléchir à l'intégration de https://borispaing.fr/doc/!monnaie/faq-index
# TODO template spécifique à la FAQ avec des questions dépliantes

+++

# FAQ

La section Foire Aux Questions, ou encore *frequently asked questions*, vise à répondre aux questions récurrentes sur Duniter, la monnaie libre, ou la monnaie ğ1 qui ne sont pas traitées dans le [wiki](@/wiki/_index.md).

Si votre question porte sur le logiciel, consultez la [FAQ Duniter](@/faq/duniter/_index.md), si elle porte sur la théorie ou l'économie, consultez la [FAQ Monnaie libre](@faq/monnaie-libre/_index.md), si elle porte sur la ğ1 consultez la [FAQ Ǧ1](@/faq/g1/_index.md). Si vous êtes perdus, c'est que nous avons mal fait notre travail. Posez-donc directement votre question sur le [forum monnaie libre](https://forum.monnaie-libre.fr/) !

{% note(type="warning") %}
La FAQ est vieillissante et nécessite une relecture approfondie et si possible une ré-écriture partielle.
{% end %}