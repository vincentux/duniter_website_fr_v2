::uml:: format="svg" alt="Diagramme Moment d'exécution d'un module"

@startuml

title Moment d'execution d'un module

onConfig -> onConfiguredExecute : onConfiguredExecute defini ?

onConfiguredExecute -> onConfiguredExecute : execution de <cmd>

onConfiguredExecute -> onConfig

onConfig -> onDatabaseExecute : onDatabaseExecute defini ?

onDatabaseExecute -> onDatabaseExecute : execution de <cmd>

onDatabaseExecute -> onConfig

@enduml

::end-uml::