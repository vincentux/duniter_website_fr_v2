::uml:: format="svg" alt="Diagramme Partage d'un signal de déconnexion"

@startuml

WS2P -> server : signal de deconnexion
server -> WS2P : signal de deconnexion (echo)

@enduml

::end-uml::