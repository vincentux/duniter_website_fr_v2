# Contenu

Le contenu est rédigé au format [Markdown](https://daringfireball.net/projects/markdown/syntax) comme le forum. Il peut inclure du html, mais veuillez ne pas en abuser. Il peut contenir des shortcodes définis dans le template.

## Liens internes

Pour faire un lien interne, le mieux et d'utiliser un lien absolu préfixé d'un `@` sous la forme suivante :

```
[lien](@/wiki/fichier.md)
```

Cela permet de vérifier à la compilation qu'aucun lien interne n'est cassé.

## Table des matières

Pour insérer une table des matières, il suffit d'écrire `[TOC]` (table of contents) à l'endroit désiré.

## Shortcodes

Les shortcodes permettent d'insérer un bout de html réutilisable. Ils sont définis dans le dossier `templates/shortcodes`, un shortcode par fichier. Par exemple :

```
{{ youtube(id="Wl959QnD3lM") }}
```

sera remplacé par :

```html
<div class="embed">
    <iframe src="https://www.youtube.com/embed/Wl959QnD3lM" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
</div>
```

Si vous pensez que votre code html peut être réutilisé, n'hésitez pas à écire un shortcode.

Autre exemple, pour une note utilisez le shortcode `note` qui prend des arguments optionnels :

- type (ok / warning / error / info) (défaut à neutral)
- display (block / icon) (défaut à both)
- markdown (true / false) (défaut à false)